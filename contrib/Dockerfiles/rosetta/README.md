# Rosetta

Adapter for Coinbase's Rosetta API system

Contains:

Dockerfile that builds Blurt and runs a rosetta API according to the Rosetta Dockerfile style guide
Javascript Rosetta-compliant API adapter

