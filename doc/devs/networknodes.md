---
title:  Network Nodes
geometry: margin=2cm
---

# Network Nodes

These are some of the currently known network nodes available in the Blurt ecosystem, please commit any changes to this document should you know of any nodes not listed or any of the currently listed ones ceasing to function. 


| Node Url              | Operator  |  
|-----------------------|-----------|
| api.blurtworld.com    | @yehey    |
| api.blurt.tools       | @blurtdev |
| blurtd.privex.io      | privex.io |  
| rpc.blurt.buzz        | @ericet   | 
| rpc.blurt.world       | Core Team | 
| api.blurt.blog        | Core Team |
